package com.cnf.dto;

import lombok.Data;

import java.time.OffsetDateTime;
@Data
public class LoginMuniAuthPeriodDTO {
    private Integer muniAuthPeriodId;
    private Integer muniCode;
    private String muniName;
    private Integer userId;
    private String userName;
    private OffsetDateTime accessGrantedDateStart;
    private OffsetDateTime accessGrantedDateStop;
    private OffsetDateTime recordDeactivatedTS;
    private String authorizedRole;
    private OffsetDateTime createdTS;
    private Integer createdByUserId;
    private String notes;
    private Integer supportAssignedBy;
    private Integer assignmentRank;
    private OffsetDateTime oathTS;
    private Integer oathCourtEntityId;
    private Boolean codeOfficer;
}
