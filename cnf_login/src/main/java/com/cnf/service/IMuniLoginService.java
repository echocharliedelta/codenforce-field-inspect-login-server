package com.cnf.service;

import com.cnf.dto.LoginMuniAuthPeriodDTO;
import com.cnf.exception.InvalidUserException;
import java.util.List;

public interface IMuniLoginService {

  boolean isValidLoginMuniAuthPeriod(int municipalityAuthPeriodId, int userId, int municipalityCode);

  List<LoginMuniAuthPeriodDTO> getLoginMuniAuthPeriodList(String userLoginToken) throws InvalidUserException;

}

