package com.cnf.service.impl;

import com.cnf.dao.MuniLoginMapper;
import com.cnf.dao.MunicipalityMapper;
import com.cnf.dao.HumanMapper;
import com.cnf.dao.UserLoginMapper;
import com.cnf.domain.LoginMuniAuthPeriod;

import com.cnf.dto.LoginMuniAuthPeriodDTO;
import com.cnf.exception.InvalidUserException;
import com.cnf.service.IMuniLoginService;
import com.cnf.service.IUserLoginService;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class MuniLoginServiceImpl implements IMuniLoginService {

  @Resource
  private MuniLoginMapper muniLoginMapper;

  @Resource
  private IUserLoginService userLoginService;

  @Resource
  private StringRedisTemplate stringRedisTemplate;

  @Autowired
  private MunicipalityMapper municipalityMapper;

  @Autowired
  private UserLoginMapper userLoginMapper;

  @Autowired
  private HumanMapper humanMapper;

  @Override
  public boolean isValidLoginMuniAuthPeriod(int municipalityAuthPeriodId, int userId,
      int municipalityCode) {
    LoginMuniAuthPeriod loginMuniAuthPeriod = muniLoginMapper.selectLoginMuniAuthPeriod(
        municipalityAuthPeriodId, userId, municipalityCode);
    return isValidLoginMuniAuthPeriod(loginMuniAuthPeriod);
  }

  @Override
  public List<LoginMuniAuthPeriodDTO> getLoginMuniAuthPeriodList(String loginUserToken)
      throws InvalidUserException {
    Integer userId = userLoginService.isValidUser(loginUserToken);
    ModelMapper modelMapper = new ModelMapper();
    List<LoginMuniAuthPeriod> loginMuniAuthPeriodList = muniLoginMapper.selectLoginMuniAuthPeriodList(userId);
      return loginMuniAuthPeriodList.stream()
            .map(loginMuniAuthPeriod -> {
              LoginMuniAuthPeriodDTO dto = modelMapper.map(loginMuniAuthPeriod, LoginMuniAuthPeriodDTO.class);
              String muniname = municipalityMapper.getMuninameByMunicode(dto.getMuniCode());
              dto.setMuniName(muniname);
              dto.setUserName(getInspectorUserName(loginMuniAuthPeriod.getUserId()));
              return dto;
            })
            .collect(Collectors.toList());
  }

  private boolean isValidLoginMuniAuthPeriod(LoginMuniAuthPeriod loginMuniAuthPeriod) {
    if (loginMuniAuthPeriod == null
        || loginMuniAuthPeriod.getAccessGrantedDateStop() == null
        || loginMuniAuthPeriod.getAccessGrantedDateStart() == null) {
      return false;
    }
    OffsetDateTime syncNow = OffsetDateTime.now();
    if (loginMuniAuthPeriod.getRecordDeactivatedTS() != null
        || loginMuniAuthPeriod.getAccessGrantedDateStart().isAfter(syncNow)
        || loginMuniAuthPeriod.getAccessGrantedDateStop().isBefore(syncNow)) {
      return false;
    }
    return true;
  }

  public String getInspectorUserName(Integer userId) {

    Integer humanId = userLoginMapper.selectHumanId(userId);
    if (Objects.isNull(humanId)) {
      return null;
    }

    String humanName = humanMapper.selectHumanName(humanId);
    return humanName;
  }
}