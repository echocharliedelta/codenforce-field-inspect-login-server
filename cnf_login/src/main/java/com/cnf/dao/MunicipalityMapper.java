package com.cnf.dao;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MunicipalityMapper {
    String getMuninameByMunicode(int municode);
}
