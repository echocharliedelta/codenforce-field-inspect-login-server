package com.cnf.dao;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HumanMapper {
    String selectHumanName(int humanId);
}
